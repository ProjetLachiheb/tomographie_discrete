# Un problème de tomographie discrète


## Auteures

Etudiantes :

* Lachiheb 	Sarah
* Leroy 		Cassandre

Sorbonne Université (UPMC) 2018

## Préambule

Le but du projet est de construire, s’il en existe, une solution (un coloriage noir-blanc des cases) répondant aux contraintes.

### Consigne et Rapport :

Fichier  |
------------- | 
[Consigne](https://gitlab.com/ProjetLachiheb/tomographie_discrete/blob/master/Rapport/enonce.pdf) |
[Rapport](https://gitlab.com/ProjetLachiheb/tomographie_discrete/blob/master/Rapport/Rapport_MOGPL_pdf.pdf) |

## Codes sources

Fichier| 
------------- | 
[Programmation dynamique](https://gitlab.com/ProjetLachiheb/tomographie_discrete/blob/master/sources/Programmation_dynamique.py) |
[PLNE](https://gitlab.com/ProjetLachiheb/tomographie_discrete/blob/master/sources/PLNE.py) |
[Méthode globale](https://gitlab.com/ProjetLachiheb/tomographie_discrete/blob/master/sources/Methode_globale.py) |


## Instances

Fichier | 
------------- | 
[Instances d'entrainement](https://gitlab.com/ProjetLachiheb/tomographie_discrete/tree/master/instances) |
[Instances d'examen](https://gitlab.com/ProjetLachiheb/tomographie_discrete/tree/master/partiel) |
[Images résolues par PLNE](https://gitlab.com/ProjetLachiheb/tomographie_discrete/tree/master/images_PLNE) |
[Images résolues par programmation dynamique](https://gitlab.com/ProjetLachiheb/tomographie_discrete/tree/master/images_Programmation_dynamique) |
